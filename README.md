# CityHack22 Project Submission
## Project: Pythia
<img src="./logo.jpeg" width="200" alt="Project logo"/>

## Team: Jackals
## Members
- Lazar Galić (Leader)
- Momčilo Mrkaić
- Mohan Rahil Raj
- Dutta Shreyan
- Dhal Manish 

## Description of the Project (300 words)

## 3 Most Impactful Features of the Project (with Screenshot and Short Description (150 words))
1. Nanoinsurance
2. Risk assessment
3. P2P insurance pools

## Tech used (as many as required)
1. TensorFlow
2. SageMaker
3. StatsModels library
4. SciKit Learn